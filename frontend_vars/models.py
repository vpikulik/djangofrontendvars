# -*- coding: utf-8 -*-
from django.db import models


class BaseVar(models.Model):

    key = models.CharField(max_length=40, db_index=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return self.key

    class Meta:
        abstract = True


class FrontendVar(BaseVar):

    value = models.CharField(max_length=255, null=True, blank=True)


class BigFrontendVar(BaseVar):

    value = models.TextField(null=True, blank=True)


_vars = {}


class GetVar(object):

    def __init__(self, key, big=False):

        VarModel = FrontendVar if not big else BigFrontendVar

        self.key = key
        if not key in _vars:
            try:
                _vars[key] = VarModel.objects.get(key=key).value
            except VarModel.DoesNotExist:
                new_var = VarModel(key=key)
                new_var.save()
                _vars[key] = new_var.value

    def update(self, value):
        _vars[self.key] = value

    def render(self):
        return _vars[self.key]


def change_var(sender, **kwargs):
    var = kwargs['instance']
    GetVar(var.key, big=isinstance(var, BigFrontendVar)).update(var.value)
models.signals.post_save.connect(change_var, sender=FrontendVar)
models.signals.post_save.connect(change_var, sender=BigFrontendVar)


def delete_var(sender, **kwargs):
    var = kwargs['instance']
    GetVar(var.key, big=isinstance(var, BigFrontendVar)).update('')
models.signals.pre_delete.connect(delete_var, sender=FrontendVar)
models.signals.pre_delete.connect(delete_var, sender=BigFrontendVar)
