from django import template
from frontend_vars.models import GetVar

register = template.Library()


def get_var(key, big, default):
    value = GetVar(key, big=big).render()
    if not value:
        return default
    return value


@register.simple_tag()
def fvar(key, default=''):
    return get_var(key, big=False, default=default)


@register.simple_tag()
def bigfvar(key, default=''):
    return get_var(key, big=True, default=default)
