from django.contrib import admin

from models import FrontendVar,BigFrontendVar


class FrontendVarAdmin(admin.ModelAdmin):

    list_display = ('key', 'value', 'description')
    list_display_links = ('key',)
    list_editable = ('value',)
    readonly_fields = ('key',)

    fieldsets = (
        (None, {
            'fields': ('key', 'value', 'description',)
        }),
    )


class BigFrontendVarAdmin(admin.ModelAdmin):

    list_display = ('key', 'value', 'description')
    list_display_links = ('key',)
    readonly_fields = ('key',)

    fieldsets = (
        (None, {
            'fields': ('key', 'value', 'description',)
        }),
    )


admin.site.register(FrontendVar, FrontendVarAdmin)
admin.site.register(BigFrontendVar, BigFrontendVarAdmin)
