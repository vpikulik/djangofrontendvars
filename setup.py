#!/usr/bin/env python

from distutils.core import setup

setup(name='DjangoFrontendVars',
      version='0.1',
      description='Django frontend vars',
      author='Vitaly Pikulik',
      author_email='v.pikulik@gmail.com',
      url='https://bitbucket.org/vpikulik/djangofrontendvars',
      packages=['frontend_vars', 'frontend_vars.templatetags', ],
     )